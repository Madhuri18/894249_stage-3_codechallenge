package com.restapi.mstock.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.restapi.mstock.models.UserDetails;

public class UserResultSetExtractor implements ResultSetExtractor<UserDetails>{

	@Override
	public UserDetails extractData(ResultSet rs) throws SQLException, DataAccessException {

		UserDetails user = null;
		if(rs.next()) {
			user = new UserDetails();
			user.setUserid(rs.getInt(1));
			user.setUsername(rs.getString(2));
			user.setEmail(rs.getString(3));
			user.setPassword(rs.getString(4));
			user.setMobile(rs.getString(5));
		}
		return user;
	}

}
